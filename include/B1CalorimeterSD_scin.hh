#ifndef B1CalorimeterSD_h_scin
#define B1CalorimeterSD_h_scin 1
#include "G4VSensitiveDetector.hh"
#include <map>

#include "B1CalorHit.hh"
//now we use rootmgr or IO, skip calohit vector
#include "RootManager.hh"

#include <vector>
class G4Step;
class G4HCofThisEvent;

class B1CalorimeterSD_scin : public G4VSensitiveDetector
{
    public:
        B1CalorimeterSD_scin(const G4String& name, RootManager *rootMng);
        virtual ~B1CalorimeterSD_scin();

        virtual void   Initialize(G4HCofThisEvent* hitCollection);
        virtual G4bool ProcessHits(G4Step*Step, G4TouchableHistory*ROhist);
        virtual void   EndOfEvent(G4HCofThisEvent* hitCollection);

        // void clean();

    private:

        B1CalorHitsCollection* fHitsCollection;
        G4int   fNofCells;
        RootManager* fRootMgr;


        int bar_num ;
        std::vector<double>edep_perbar;
        std::vector<double>bar_edep;
        // int eID_scin;
        G4double  eEnergy_scin;
        // double  eTime_sci;
        int     nPhoton_scin; 
        int     copy_num;
        int     layer_total;
        // int     layer_num;
        int     layer_nu = 78 ;     
        int     layer_id;
        G4double  layer_energy;
        std::vector<double> layer_edep= std::vector<double>(layer_nu , 0);
        int     bar_id;
        int     xy_id;
        double  perbar_edep_x[80][25]={0};
        double  perbar_edep_y[80][25]={0};
        std::vector<double> edep_bar_x;
        std::vector<double> edep_bar_y;
        //  std::vector<vector<double>> edep_bar_x(78,vector<double>(20));
        // std::vector<vector<double>> edep_bar_y(78,vector<double>(20));
        ////////
     
        int eID;
        double eEnergy;
        double eTime;
        int nPhoton;
        // int layer_id;
        int layer_num = 78 ;
        std::vector<int> photon_num = std::vector<int>(layer_num,0);
};
#endif