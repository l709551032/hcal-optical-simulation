#include "B1CalorimeterSD_scin.hh"
#include "G4HCofThisEvent.hh"
#include "G4Step.hh"
#include "G4ThreeVector.hh"
#include "G4SDManager.hh"
#include "G4ios.hh"

#ifdef G4MULTITHREADED
#include "G4MTRunManager.hh"
#else
#include "G4RunManager.hh"
#endif
#include "G4Event.hh"

B1CalorimeterSD_scin::B1CalorimeterSD_scin(const G4String& name, RootManager *rootMng) : G4VSensitiveDetector(name),
fRootMgr(rootMng)
{
    G4cout << "SensitiveDetector Processed Successfully " << G4endl;
}

B1CalorimeterSD_scin::~B1CalorimeterSD_scin()
{
}

void B1CalorimeterSD_scin::Initialize(G4HCofThisEvent* hce)
{ 
//   bar_num = 1570;
  eEnergy_scin =0 ;
//   copy_num = 0;
// edep_perbar = std::vector<double>(bar_num,0);
  // layer_edep = std::vector<double>(layer_nu , 0);
   eID=-1;
  eEnergy=0.;
  eTime=-1.;
  nPhoton=0;
}
G4bool B1CalorimeterSD_scin::ProcessHits(G4Step* step, G4TouchableHistory*ROhist)
{ 
  // G4cout << "[SD] ==> Catch one hit \n" <<G4endl;
  if(!step->GetPreStepPoint())
  {
    G4cout << "GetPrestepPoint stop.";
    return false;
    
  }
  // layer_id = step->GetPreStepPoint()->GetTouchableHandle()->GetCopyNumber(2);
  //auto *touchable = (G4TouchableHistory *) (step->GetPreStepPoint()->GetTouchable());
  layer_id = step->GetPreStepPoint()->GetTouchableHandle()->GetReplicaNumber(3);
  //     copy_num = step->GetPreStepPoint()->GetTouchableHandle()->GetCopyNumber();
  
  // // std::cout << "copy number: " << copy_num <<std::endl;
  bar_id = step->GetPreStepPoint()->GetTouchableHandle()->GetReplicaNumber(1);
  eEnergy_scin  = step->GetTotalEnergyDeposit();
  xy_id = step->GetPreStepPoint()->GetTouchableHandle()->GetReplicaNumber(2);
  // bar_edep.push_back(eEnergy_scin);

  //eEnergy_scin = step->GetTrack()->GetTotalEnergy();
  // edep_perbar.at(copy_num) += eEnergy_scin;
  if(xy_id == 0){
    perbar_edep_x[layer_id][bar_id] += eEnergy_scin;
  }
  else if(xy_id==1){
    perbar_edep_y[layer_id][bar_id] += eEnergy_scin;
  }
  // std::cout << "Layer id: " << layer_id << "  " << "bar_id : " << bar_id << " bar_ededp: " << eEnergy_scin << "\n";
  layer_edep.at(layer_id) += eEnergy_scin;
  // if (eEnergy_scin < 0)std::cout <<"eEne0less exists"<< "\n";
  // std::cout << "layer_id : " << layer_id << std::endl;
  // std::cout << "eEnergy_scin : " << eEnergy_scin <<"MeV" << std::endl;
  // std::cout << "Pos : " << step->GetPreStepPoint()->GetPosition().z() << " mm" << std::endl;
  // std::cout << "PDG : " << step->GetTrack()->GetDefinition()->GetPDGEncoding() << std::endl;
  
  //  const G4Event* event =  G4RunManager::GetRunManager()->GetCurrentEvent();
  // if(event)
  // {
  //   eID=event->GetEventID();
  // }
  // eEnergy=step->GetTrack()->GetTotalEnergy();
  // eTime=step->GetPreStepPoint()->GetGlobalTime();
  // G4String name = step->GetTrack()->GetDefinition()->GetParticleName();
  // G4String process = step->GetTrack()->GetCreatorProcess()->GetProcessName();
 
  // // G4cout<< "name is "<< name << G4endl;
  // if (name=="opticalphoton"&&process=="Scintillation")
  ////Kill the optical process
  // if (name=="opticalphoton")
  // {
  //     nPhoton+=1;  
  //     layer_id = step->GetPreStepPoint()->GetTouchableHandle()->GetReplicaNumber(3);
  //     // std::cout << "Photon layer id: " << layer_id << std::endl;
  //     photon_num.at(layer_id)+=1;
  //     step->GetTrack()->SetTrackStatus(fKillTrackAndSecondaries);
  // }
  
  //fEventAction->AddSipmEdep(eEnergy);
  //fEventAction->GetSipmTime(eTime);

  //fRootMgr->FillSipmPhoton(step->GetTrack()->GetTotalEnergy(),  step->GetPreStepPoint()->GetGlobalTime(), eID);
  return true;
}

void B1CalorimeterSD_scin::EndOfEvent(G4HCofThisEvent*)
{
      
     for(int i = 0 ; i < 20 ; i++){
        for (int j = 0 ; j < 78 ; j++){
            edep_bar_x.push_back(perbar_edep_x[j][i]);
            // if(perbar_edep_x[j][i] < 0 )std::cout<<"barE0less exists" << "\n";
            perbar_edep_x[j][i] = 0;
        }
    }
     for(int i = 0 ; i < 20 ; i++){
        for (int j = 0 ; j < 78 ; j++){
            edep_bar_y.push_back(perbar_edep_y[j][i]);
            perbar_edep_y[j][i] = 0 ;
        }
    }
  //fRootMgr->FillSipmPhoton(step->GetTrack()->GetTotalEnergy(),  step->GetPreStepPoint()->GetGlobalTime(), eID);
  fRootMgr->FillScinEdep(layer_edep ,edep_bar_x, edep_bar_y);
    //  for(int i = 0 ; i < 20 ; i++){
    //     for (int j = 0 ; j < 78 ; j++){
    //         perbar_edep[j][i] = 0;
    //     }
    // }
  // fRootMgr->FillSipmPhoton(eEnergy, eTime, nPhoton, eID,photon_num);

  // std::fill(photon_num.begin(), photon_num.end(), 0);
  // eEnergy=0;
  // eTime=0;
  // nPhoton=0;
  // eID=0;
  edep_bar_x.clear();
  edep_bar_y.clear();
  std::fill(layer_edep.begin(), layer_edep.end(), 0);
  // std::fill(edep_bar_x.begin(), edep_bar_x.end(), 0);
  // std::fill(edep_bar_y.begin(), edep_bar_y.end(), 0);
//   for(int i = 0 ; i < edep_bar_x.size() ;i++){
//   std::fill(edep_bar_x[i].begin(), edep_bar_x[i].end(), 0);
//   std::fill(edep_bar_y[i].begin(), edep_bar_y[i].end(), 0);
//   }
}



